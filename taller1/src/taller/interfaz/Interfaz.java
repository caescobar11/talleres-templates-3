package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
       //TODO
		
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		
		System.out.println("Ingrese número de jugadores");
		
		int numeroJugadores = sc.nextInt();
		
		for (int i = 0; i < numeroJugadores; i ++)
		{
		System.out.println("Ingrese nombre jugador" + i);
		
		String nombre = sc.next();
		
		System.out.println("Ingrese simbolo jugador"+i);
		
		String simbolo1 = sc.next();
		
		
		Jugador jugador = new Jugador(nombre, simbolo1);
		jugadores.add(jugador);
		
		}
		
		
		System.out.println("-Cuantas columnas quiere? -");
		
		int col = sc.nextInt();
		
		
		System.out.println("-Cuantas filas quiere?-");
		
		int fil = sc.nextInt();
		
		juego = new LineaCuatro(jugadores, fil, col);
		
	
		while (!juego.fin())
		{
			imprimirTablero();
			
			System.out.println("En que columna quiere jugar?");
			
			int colJug = sc.nextInt();
			
			juego.registrarJugada(colJug);
			
			
		}
		
		
		
		
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
		
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
			

		System.out.println("Ingrese nombre jugador");
		
		String nombre = sc.next();
		
		System.out.println("Ingrese simbolo jugador");
		
		String simbolo = sc.next();
		
		Jugador jugador = new Jugador (nombre,simbolo);
		
		jugadores.add(jugador);
		
		Jugador maquina = new Jugador ("Maquina","M");
		jugadores.add(maquina);
		
		
		
		
		
		
		System.out.println("-Cuantas columnas quiere? -");
		
		int col = sc.nextInt();
		
		
		System.out.println("-Cuantas filas quiere?-");
		
		int fil = sc.nextInt();
		
		juego = new LineaCuatro(jugadores, fil, col);
		
	
		while (!juego.fin())
		{
			imprimirTablero();
			
			System.out.println("En que columna quiere jugar?");
			
			int colJug = sc.nextInt();
			
			juego.registrarJugada(colJug);
			
			juego.registrarJugadaAleatoria();
			
			
		}
		
		
	}
	
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		//TODO
		
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		String[][] tablero = juego.darTablero();
		for (int i = 0 ; i < tablero.length; i ++)
		{
			for (int j = 0; j < tablero[0].length; j ++)
			{
				System.out.print(tablero[i][j]);
			}
			
			System.out.print("\n");
		}
			
	}
}
