package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	public Jugador buscarJugador(String nombre)
	{
		Jugador j = null;
		for (int i = 0; i < jugadores.size(); i++)
		{
			if (jugadores.get(i).darNombre().equals(nombre))
			{
				j = jugadores.get(i);
			}
		}
		
		return j;
	}
	
	public boolean seHaJugado(int col, int fil)
	{
		if (tablero[col][fil].equals("___"))
			return false;
		else return true;
	}
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		int columnas = tablero.length;
		
		Jugador j = buscarJugador(atacante);
		int columnaJugada = (int) (Math.random() * columnas) - 1;
		boolean seHaPuesto = false;
		for (int i = 0; i < tablero[0].length && !seHaPuesto; i++)
		{
				if (!seHaJugado(columnaJugada, i ))
				{
					tablero[columnaJugada][i] = "_" + j.darSimbolo() + "_";
					seHaPuesto = true;
				}
		}
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO
		
		int columnas = tablero.length;
		boolean seHaPuesto = false;
		
		if (col < columnas && col >= 0)
		{
		Jugador j = buscarJugador(atacante);
		
		
		for (int i = 0; i < tablero[0].length && !seHaPuesto; i++)
		{
				if (!seHaJugado(col, i ))
				{
					tablero[col][i] = "_" + j.darSimbolo() + "_";
					seHaPuesto = true;
				}
		}
		}
		return seHaPuesto;
	}
	
	
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
		
		boolean seTermina = false;
	
		
		
		return seTermina;
	}

	public void imprimirTablero()
	{
		//TODO
		
		for (int i = 0 ; i < tablero.length; i ++)
		{
			for (int j = 0; j < tablero[0].length; j ++)
			{
				System.out.print(tablero[i][j]);
			}
			
			System.out.print("\n");
		}
			
	}


}
